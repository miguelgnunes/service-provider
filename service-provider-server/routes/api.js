var utils = require("../utils")

var express = require('express');
var router = express.Router();
const { query, validationResult } = require('express-validator');

const MINIMUM_SKILLS = 3;

router.get('/requests', [
  query("skills").exists(),
  query("skills").isArray({min: MINIMUM_SKILLS}),
], function(req, res, next) {
  const errors = validationResult(req);
  if(!errors.isEmpty())
    return res.status(422).send("Invalid parameters");

  let requests = utils.loadResourceJSON("requests.json")
  res.send(utils.mapRequestsWithSkills(requests, req.query.skills))

});

module.exports = router;
