var fs = require("fs");
var path = require("path");
var _ = require("lodash")

module.exports = {
  loadResourceJSON(fileName) {
    let content = fs.readFileSync(path.join(__dirname,"../resources/", fileName));
    return JSON.parse(content);
  },
  mapRequestsWithSkills(requests, skills) {
    return _.map(requests, request => {
      let req = Object.assign({}, request);
      req.skills = _.sampleSize(skills, Math.ceil(Math.random() * 4));
      return req;
    })
  }
}