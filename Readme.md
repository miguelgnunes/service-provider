## Service Provider App

*Author: Miguel Gama Nunes*

This project is the outcome of the assignment provided by Lepaya. It is not in a usable state, particularly since it uses dummy data and does not persist user interaction.


### How to run project:

1. Install all dependencies of both server and client projects. From the project root folder, run

    `$> cd service-provider-app && yarn install && cd ../service-provider-server && yarn install`

2. Run following command

    `$> yarn serve`

    This last command will run an Express Nodejs server on port 3000 and vue-cli dummy server on port 8080 concurrently

3. You can now visit the app at [http://localhost:8080/]