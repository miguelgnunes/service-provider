//This class is optimized to lookup skills, not so much for iterating over them
//The main use case for this class is to verify whether a skill was already chosen
//The secondary use case is to, when submitting (once per update of all skills), get list of all skill.
// This solution is not optimal for second use case, but still acceptable. A way to optimize it in case it was read-heavy
// rather than write-heavy would be to keep record of an array that keeps track of all the keys (this case, skill names)

export default class SkillSet {
        constructor() {
                this.skillExperienceMap = {};
        }

        addSkill(skill, experience) {
                //Even if skill already exists, we simply overwrite it
                this.skillExperienceMap[skill] = experience;
        }

        removeSkill(skill) {
                //Check if skill was already set. If yes, we delete it
                if(skill in this.skillExperienceMap)
                        delete this.skillExperienceMap[skill];
        }

        hasSkill(skill) {
                return skill in this.skillExperienceMap;
        }

        getExperience(skill) {
                if(skill in this.skillExperienceMap)
                        return this.skillExperienceMap[skill];
                return null;
        }

        getSkillExperienceMap() {
                return this.skillExperienceMap;
        }

        getSkills() {
                return Object.keys(this.skillExperienceMap);
        }

        encodeSkillsURI() {
                let encoded = "";
                let skills = this.getSkills();
                for(let i = 0; i < skills.length; i++) {
                        let skill = skills[i];
                        if(i !== 0)
                                encoded += "&";
                        encoded +=  "skills[]=" + encodeURI(skill);
                }
                return encoded;
        }
}