var options = {
  baseURL: "api/",
};

window.env = process.env;
if(process.env.NODE_ENV === "development")
  options = {
    ...options,
  };
else if(process.env.NODE_ENV === "staging")
  options = {
    ...options,
  };

export default options;