import exampleRequestsResponse from "../assets/exampleRequestsResponse";
import * as _ from 'lodash'

export default {
  methods: {
    generateRequests(skills) {
      return _.map(exampleRequestsResponse, request => {
        let req = Object.assign({}, request);
        req.skills = _.sampleSize(skills, Math.ceil(Math.random() * 4));
        return req;
      })
    }
  },
  filters: {

  }

}
