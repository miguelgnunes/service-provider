import options from "./options";

const axiosInstance = require("axios").create({
  baseURL: options.baseURL,
  timeout: 120000,
  headers: {
    'Content-Type': 'application/json',
  }
})

window.axios = axiosInstance;