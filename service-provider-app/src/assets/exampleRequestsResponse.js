export default [
  {
    "id": 1,
    "title": "Example request 1",
    "description": "Example description 1",
    "skills": [],
    "startDate": "2020-03-10",
    "endDate": "2020-03-20"
  },
  {
    "id": 2,
    "title": "Example request 2",
    "description": "Example description 2",
    "skills": [],
    "startDate": "2020-03-12",
    "endDate": "2020-03-24"
  },
  {
    "id": 3,
    "title": "Example request 3",
    "description": "Example description 3",
    "skills": [],
    "startDate": "2020-02-01",
    "endDate": "2020-02-08"
  },
  {
    "id": 4,
    "title": "Example request 4",
    "description": "Example description 4",
    "skills": [],
    "startDate": "2020-03-10",
    "endDate": "2020-03-20"
  },
  {
    "id": 5,
    "title": "Example request 5",
    "description": "Example description 5",
    "skills": [],
    "startDate": "2020-04-12",
    "endDate": "2020-05-24"
  }
]